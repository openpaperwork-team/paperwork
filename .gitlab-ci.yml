# image: $CI_REGISTRY_IMAGE/build:latest
image: registry.gitlab.gnome.org/world/openpaperwork/paperwork/build:latest

variables:
  GIT_STRATEGY: clone
  GIT_SUBMODULE_STRATEGY: none
  GIT_FETCH_EXTRA_FLAGS: --tags

stages:
  # no point in waiting for the tests to end before generating the data files
  # or the development documentation
  - build_img
  - tests
  - data
  - deploy

build_img:
  stage: build_img
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
      - ci/Dockerfile
    - if: $CI_COMMIT_BRANCH == "testing"
      changes:
      - ci/Dockerfile
    - if: $CI_COMMIT_BRANCH == "develop"
      changes:
      - ci/Dockerfile
  tags:
    - openpaper-flatpak
  script:
    # make sure to fetch the latest Debian image
    - docker system prune -f
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE/build:latest ci
    - docker push $CI_REGISTRY_IMAGE/build:latest

check:
  stage: tests
  script:
    - source ./activate_test_env.sh
    - make check


test:
  stage: tests
  script:
    - apt-get update
    - source ./activate_test_env.sh
    - pip3 install -U setuptools
    - PIP_DEPS=[dev] make install
    - paperwork-gtk chkdeps -y
    - paperwork-cli chkdeps -y
    - make test


test_chkdeps:
  image: debian:testing
  stage: tests
  script:
    - apt-get update
    # bare minimum to install Paperwork from sources
    - apt-get install -y
        build-essential
        gettext
        git
        libcairo2-dev
        libgirepository1.0-dev
        make
        python3
        python3-dev
        python3-pip
        python3-virtualenv
        wget
    - virtualenv -p python3 /venv
    - source /venv/bin/activate
    - pip3 install -U setuptools
    - make install
    - paperwork-json chkdeps


generate_data:
  stage: data
  script:
    - apt-get update
    - source ./activate_test_env.sh
    - pip3 install -U setuptools
    - make install
    - xvfb-run -a -e /dev/stderr paperwork-gtk chkdeps -y
    - xvfb-run -a -e /dev/stderr paperwork-cli chkdeps -y
    - make data upload_data


doc_devel:
  stage: data
  script:
    - source ./activate_test_env.sh
    - pip3 install -U setuptools
    - make doc
    - make upload_doc


linux_flatpak:
  stage: deploy
  timeout: 48h
  only:
    - branches@World/OpenPaperwork/paperwork
    - tags@World/OpenPaperwork/paperwork
  tags:
    - openpaper-flatpak
  script:
    # workaround error 'fatal: transport 'file' not allowed'
    - git config --global --add protocol.file.allow always
    # Running in from a gitlab-runner directly in a shell, as the user
    # 'gitlab-runner'
    # --> not running as root, so we cannot actually install anything
    # - apt-get update
    # - apt-get install -y -q rsync flatpak-builder make jq moreutils
    - ./ci/update_flatpak_repo.sh

linux_appimage:
  stage: deploy
  timeout: 3h
  tags:
    - fuse
  script:
    - apt-get install -y libfuse2t64
    - virtualenv -p python3 /venv
    - source /venv/bin/activate
    - pip3 install -U setuptools
    - pip3 install appimage-builder
    # Build
    - make linux_exe
    # test the build
    - paperwork-gtk/Paperwork-gtk-latest-x86_64.AppImage ping
    - ./ci/deliver.sh paperwork-gtk/Paperwork-gtk-latest-x86_64.AppImage linux .appimage paperwork-gtk
    - ./ci/deliver.sh paperwork-shell/Paperwork-cli-latest-x86_64.AppImage linux .appimage paperwork-cli
    - ./ci/deliver.sh paperwork-shell/Paperwork-json-latest-x86_64.AppImage linux .appimage paperwork-json
  artifacts:
    expire_in: 2 days
    paths:
      - paperwork-gtk/Paperwork-gtk-latest-x86_64.AppImage
      - paperwork-shell/Paperwork-cli-latest-x86_64.AppImage
      - paperwork-shell/Paperwork-json-latest-x86_64.AppImage


.windows: &windows
  variables:
    MSYSTEM: "MINGW64"
    CHERE_INVOKING: "yes"
  before_script:
    # Libinsane build dependencies
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S make
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-cunit
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-doxygen
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-gcc
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-gobject-introspection
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-meson
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-gobject
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-vala
    # Paperwork build dependencies
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S git  # for 'make version'
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-ca-certificates
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-cairo
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-gdk-pixbuf2
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-gettext
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-gtk3
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-libhandy
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-libnotify
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-nsis
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-nsis-nsisunz
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-poppler
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-cairo
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-cx-freeze
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-psutil
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-pillow
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-pip
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-scikit-learn
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-setuptools
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-virtualenv
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S wget  # for downloading data files
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S zip unzip
    # do not use the checkout from gitlab-runner. For some unknown reason, it mess up
    # setuptools_scm
    - c:\msys64\usr\bin\bash -lc "rm -rf /tmp/paperwork"
    - c:\msys64\usr\bin\bash -lc "rm -rf /tmp/venv"
    - c:\msys64\usr\bin\bash -lc "cd /tmp && git clone https://gitlab.gnome.org/World/OpenPaperwork/paperwork.git -b $CI_COMMIT_REF_NAME"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && git describe --tags --always"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && git submodule init"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && git submodule update --recursive --remote"
    # libinsane is not a Python library and therefore is installed system-wide
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && make -C sub/libinsane uninstall PREFIX=/mingw64 || true"
    # actual install
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && make -C sub/libinsane install PREFIX=/mingw64"
    - c:\msys64\usr\bin\bash -lc "virtualenv --system-site-packages /tmp/venv"
    # Workaround for pycountry and Cx_freeze
    # See https://github.com/marcelotduarte/cx_Freeze/issues/930
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && . /tmp/venv/bin/activate && pip3 uninstall -y pycountry"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && . /tmp/venv/bin/activate && pip3 install --no-cache --use-pep517 pycountry==20.7.3"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && . /tmp/venv/bin/activate && make -C sub/libpillowfight install_py"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && . /tmp/venv/bin/activate && make -C sub/pyocr install_py"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && . /tmp/venv/bin/activate && make install"


windows_tests:
  stage: tests
  only:
    - branches@World/OpenPaperwork/paperwork
    - tags@World/OpenPaperwork/paperwork
  tags:
    - windows
    - msys2
  <<: *windows
  script:
    # Tesseract (required for unit tests)
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-libarchive  # missing tesseract dependency
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-python-pytest
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-tesseract-ocr
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-tesseract-data-eng
    - c:\msys64\usr\bin\pacman --needed --noconfirm -S mingw-w64-x86_64-tesseract-data-fra
    # Build
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && . /tmp/venv/bin/activate && export TESSDATA_PREFIX=/mingw64/share/tessdata && make test"


windows_exe:
  stage: deploy
  only:
    - branches@World/OpenPaperwork/paperwork
    - tags@World/OpenPaperwork/paperwork
  tags:
    - windows
    - msys2
  <<: *windows
  artifacts:
    expire_in: 2 days
    paths:
      - paperwork.zip
  script:
    # We need rclone to upload the files on OVH object storage
    - c:\msys64\usr\bin\rm -f rclone-v1.53.3-windows-386.zip
    - c:\msys64\usr\bin\rm -rf rclone-v1.53.3-windows-386
    - c:\msys64\usr\bin\wget -q https://github.com/rclone/rclone/releases/download/v1.53.3/rclone-v1.53.3-windows-386.zip
    - c:\msys64\usr\bin\unzip rclone-v1.53.3-windows-386.zip
    - c:\msys64\usr\bin\cp rclone-v1.53.3-windows-386/rclone.exe /usr/bin
    # Build
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && . /tmp/venv/bin/activate && pip install cx_Freeze"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && . /tmp/venv/bin/activate && make windows_exe"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork/build/exe && paperwork-gtk ping"
    - c:\msys64\usr\bin\bash -lc "cd /tmp/paperwork && ./ci/deliver.sh dist/paperwork.zip windows .zip"
    - c:\msys64\usr\bin\bash -lc "mv /tmp/paperwork/dist/paperwork.zip ."
